package example.annotation;

public class NullFieldException extends Exception{
    public NullFieldException(String message) {
        super(message);
    }
}
