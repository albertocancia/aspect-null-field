package example.annotation;

import org.springframework.stereotype.Component;

@Component
public class ServiceTemp {
    public void exampleMethod(@NullEmptyFields(notNullableOrEmpty = {
            "firstName",
            "lastName"
    }) Person person) throws NullFieldException{
        System.out.println("inside");
    }
}
