package example.annotation;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;

import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;

@Aspect
@Component
@Slf4j
public class TempAspect {

    @Before("execution(* *(@example.annotation.NullEmptyFields (*)))")
    public void before(JoinPoint joinPoint) throws IllegalAccessException, NullFieldException, InvocationTargetException {
        Parameter[] parameters = ((MethodSignature) joinPoint.getSignature()).getMethod().getParameters();
        Object[] objectsParameters = joinPoint.getArgs();
        for (int i = 0; i < parameters.length; i++) {
            Parameter parameter = parameters[i];
            Annotation[] annotationsOfParameter = parameter.getAnnotations();
            for (Annotation annotation : annotationsOfParameter) {
                if(annotation instanceof NullEmptyFields){
                    log.info("{}", parameter);
                    NullEmptyFields nullEmptyFields = (NullEmptyFields) annotation;
                    if(nullEmptyFields.notNullableOrEmpty().length>0){
                        reflectiveNullOrEmptyChecker(objectsParameters[i], nullEmptyFields.notNullableOrEmpty());
                    }
                    if(nullEmptyFields.notNullable().length>0){
                        reflectiveNullChecker(objectsParameters[i], nullEmptyFields.notNullable());
                    }
                    if(nullEmptyFields.notEmpty().length>0){
                        reflectiveEmptyChecker(objectsParameters[i], nullEmptyFields.notEmpty());
                    }
                }
            }
        }


    }

    private void reflectiveNullOrEmptyChecker(Object obj, String[] listOfFields) throws InvocationTargetException, IllegalAccessException, NullFieldException {
        Class classObj = obj.getClass();
        Method[] methods = classObj.getMethods();
        for (Method method : methods) {
            for (int i = 0; i < listOfFields.length; i++) {
                if (method.getName().equalsIgnoreCase("get" + listOfFields[i]) && method.getParameterCount() == 0) {
                    Object value = method.invoke(obj);
                    if (value == null || (value instanceof String && ((String) value).trim().isEmpty())) {
                        throw new NullFieldException("Null or empty field exception");
                    }
                }
            }
        }

    }

    private void reflectiveNullChecker(Object obj, String[] listOfFields) throws InvocationTargetException, IllegalAccessException, NullFieldException {
        Class classObj = obj.getClass();
        Method[] methods = classObj.getMethods();
        for (Method method : methods) {
            for (int i = 0; i < listOfFields.length; i++) {
                if (method.getName().equalsIgnoreCase("get" + listOfFields[i]) && method.getParameterCount() == 0) {
                    Object value = method.invoke(obj);
                    if (value == null) {
                        throw new NullFieldException("Null field exception");
                    }
                }
            }
        }
    }

    private void reflectiveEmptyChecker(Object obj, String[] listOfFields) throws InvocationTargetException, IllegalAccessException, NullFieldException {
        Class classObj = obj.getClass();
        Method[] methods = classObj.getMethods();
        for (Method method : methods) {
            for (int i = 0; i < listOfFields.length; i++) {
                if (method.getName().equalsIgnoreCase("get" + listOfFields[i]) && method.getParameterCount() == 0) {
                    Object value = method.invoke(obj);
                    if (value instanceof String && ((String) value).trim().isEmpty()) {
                        throw new NullFieldException("Empty field exception");
                    }
                }
            }
        }
    }
}
