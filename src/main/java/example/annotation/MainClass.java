package example.annotation;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MainClass {



    public static int test(@NullEmptyFields(notNullableOrEmpty = {"firstName", "lastName"}) Person person){
        System.out.println("In test");
        return 0;
    }

    public static void main(String[] args) {
        SpringApplication.run(MainClass.class, args);

        ServiceTemp temp = new ServiceTemp();
    }
}
