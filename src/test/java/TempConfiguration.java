import example.annotation.ServiceTemp;
import example.annotation.TempAspect;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

@Configuration
@ComponentScan("example.annotation")
public class TempConfiguration {
}
