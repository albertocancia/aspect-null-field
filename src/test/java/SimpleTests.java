import example.annotation.NullFieldException;
import example.annotation.Person;
import example.annotation.ServiceTemp;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest(classes = TempConfiguration.class)
public class SimpleTests{

    @Autowired
    ServiceTemp serviceTemp;
    @Test
    void firstNameEmtpy() throws NullFieldException {
        Person person = new Person(1L, "","dd");
        assertThrows(NullFieldException.class, () -> serviceTemp.exampleMethod(person));
    }


    @Test
    void lastNameEmtpy() throws NullFieldException {
        Person person = new Person(1L, "Alberto","  ");
        assertThrows(NullFieldException.class, () -> serviceTemp.exampleMethod(person));
    }

    @Test
    void firstNameNull() throws NullFieldException {
        Person person = new Person(1L, null,"Jj");
        assertThrows(NullFieldException.class, () -> serviceTemp.exampleMethod(person));
    }

    @Test
    void lastNameNull() throws NullFieldException {
        Person person = new Person(1L, "Alberto",null);
        assertThrows(NullFieldException.class, () -> serviceTemp.exampleMethod(person));
    }



}
